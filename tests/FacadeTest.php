<?php

namespace Duna\Plugin\Manager\Tests;

use Doctrine\ORM\EntityManager;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\Facade;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{
    /** @var Facade */
    protected $facade;
    protected $em;

    public function testInsert()
    {
        $this->em = $this->createMockBuilderEm()->getMock();
        $this->em->expects($this->once())
            ->method('persist');
        $this->em->expects($this->once())
            ->method('flush');

        $this->initProperties();
        $result = $this->facade->insert("test", 'popis');
        $this->assertNotNull($result);
        $this->isTrue($result instanceof Plugin);
        $this->assertEquals(md5("test"), $result->hash);
    }

    public function createMockBuilderEm()
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['persist', 'flush']);
    }

    public function initProperties()
    {
        $this->facade = new Facade($this->em);
    }
}