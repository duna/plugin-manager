<?php

namespace Duna\Plugin\Manager;


interface IPlugin
{
    /**
     * @return [
     *         'ext' => Entity\Plugin
     *         'install' => IInstallable::class
     *         'entity' => [DatabseEntityMapping]
     * ]
     *
     */
    public static function getPluginInfo();
}