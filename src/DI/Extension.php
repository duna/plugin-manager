<?php

namespace Duna\Plugin\Manager\DI;

use App;
use Composer\Autoload\ClassLoader;
use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Manager\IPlugin;
use Duna\Plugin\Manager\Registrator;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\DI\Compiler;
use Nette\Reflection\ClassType;

class Extension extends CompilerExtension implements IEntityProvider, IEntityConsoleProvider
{
    public static $pluginNames = [];
    private $commands = [
        \Duna\Plugin\Manager\Cli\Commands\InstallCommand::class,
    ];

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();
        $builder->getDefinition($builder->getByType(Registrator::class))
            ->addSetup(''
                . '?->onPluginRegistration[] = function ($registrator) {' . "\n"
                . "\t" . '$registrator->registerPlugin(?);' . "\n"
                . '}', ['@self', self::$pluginNames]);
    }

    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $this->parseConfig(__DIR__ . '/config.neon');
        $this->addCommands($this->commands);
    }

    public static function load(Compiler $compiler, ClassLoader $loader)
    {
        $iterator = 0;
        foreach ($loader->getClassMap() as $class => $file) {
            $reflection = ClassType::from($class);
            if ($reflection->implementsInterface(IPlugin::class) && $reflection->isInstantiable()) {
                self::$pluginNames[] = $class;
                $compiler->addExtension('dynamic.plugin.' . $iterator++, new $class);
            }
        }
    }

    public static function entityMappings()
    {
        return ["Duna\\Plugin\\Manager\\Entity" => __DIR__ . '/../Entity'];
    }
}
