<?php

namespace Duna\Plugin\Manager;

use Duna\Plugin\Manager\Entity\Plugin;
use Kdyby\Doctrine\EntityManager;
use Nette\DI\Container;
use Nette\Object;
use Tracy\Debugger;

/**
 * @method onPluginRegistration(Registrator $registrator)
 * @method onInstall()
 * @method onUninstal()
 */
class Registrator extends Object
{

    /** @var \Closure[] */
    public $onPluginRegistration = [];

    /** @var EntityManager */
    private $em;

    /** @var Container */
    private $container;

    /**
     * @var array
     * @example [md5 => Plugin $extension]
     */
    private $plugins = [];

    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function registerPlugins(array $plugin)
    {
        $this->plugins = [];
        foreach ($plugin as $name) {
            $this->plugins[md5($name)] = (new $name)->getPluginInfo();
        }
    }

    public function getPlugins()
    {
        $this->onPluginRegistration($this);
        return array_map(function ($plugin) {
            return $plugin['ext'];
        }, $this->plugins);
    }

    public function installPlugin($md5)
    {
        $that = $this;
        $this->em->transactional(function () use ($that, $md5) {
            $plugin = $that->getPluginNames($md5);
            $plugin['ext']->hash = $md5;
            $that->em->persist($plugin['ext']);
            $that->em->flush($plugin['ext']);
            if (isset($plugin['install'])) {
                $installer = $that->container->getByType($plugin['install']);
                $installer->install();
                $that->em->flush();
            }
            if (isset($plugin['entity'])) {
                $metadata = array_values($plugin['entity']);
                $path = array_pop($metadata);
                $classes = [];
                foreach ($that->getAllClassNames($path) as $class) {
                    $classes[] = $that->em->getClassMetadata($class);
                }
                $schema = new SchemaTool($that->em);
                $schema->createSchema($classes);
            }
        });


    }

    public function getPluginNames($key = null)
    {
        $this->onPluginRegistration($this);
        if ($key === null)
            return $this->plugins;
        return $this->plugins[$key];
    }

    public function getAllClassNames($path)
    {
        $classes = [];
        $includedFiles = [];

        $iterator = new \RegexIterator(
            new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::LEAVES_ONLY
            ), '/^.+' . preg_quote('.php') . '$/i', \RecursiveRegexIterator::GET_MATCH
        );

        foreach ($iterator as $file) {
            $sourceFile = $file[0];

            if (!preg_match('(^phar:)i', $sourceFile)) {
                $sourceFile = realpath($sourceFile);
            }

            require_once $sourceFile;

            $includedFiles[] = $sourceFile;
        }


        $declared = get_declared_classes();

        foreach ($declared as $className) {
            $rc = new \ReflectionClass($className);
            $sourceFile = $rc->getFileName();
            if (in_array($sourceFile, $includedFiles)) {
                $classes[] = $className;
            }
        }

        return $classes;
    }

    public function uninstallPlugin($md5)
    {
        $that = $this;
        $this->em->transactional(function () use ($that, $md5) {
            $plugin = $that->getPluginNames($md5);
            $entity = $that->em->getRepository(Plugin::class)->findOneBy(['hash' => $md5]);
            if ($entity) {
                $that->em->remove($entity);
                $that->em->flush($entity);
            }
            if (isset($plugin['install'])) {
                $installer = $that->container->getByType($plugin['install']);
                $installer->uninstall();
                $that->em->flush();
            }
            if (isset($plugin['entity'])) {
                $metadata = array_values($plugin['entity']);
                $path = array_pop($metadata);
                $classes = [];
                foreach ($this->getAllClassNames($path) as $class) {
                    $classes[] = $this->em->getClassMetadata($class);
                }
                $schema = new \Doctrine\ORM\Tools\SchemaTool($this->em);
                $schema->dropSchema($classes);
            }
        });

    }

}
