<?php

namespace Duna\Plugin\Manager;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\Manager\Entity\Plugin;
use Nette\InvalidStateException;

class Facade
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function delete($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Plugin::class)->find($id);
        try {
            if ($entity === null)
                throw new InvalidStateException();
            $this->em->remove($entity);
            $this->em->flush($entity);
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    public function deleteByHash($hash, $throwException = false)
    {
        try {
            $entity = $this->getOneByHash($hash, true);
            $this->em->remove($entity);
            $this->em->flush($entity);
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }

    }

    public function getOneByHash($hash, $throwException = false)
    {
        $entity = $this->em->getRepository(Plugin::class)->findOneBy([
            'hash' => $hash,
        ]);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    /**
     * @param      $name
     * @param      $description
     * @param bool $throwException
     * @return \Duna\Plugin\Manager\Entity\Plugin|null
     */
    public function insert($name, $description, $throwException = false)
    {
        $entity = new Plugin();
        $entity->name = $name;
        $entity->hash = md5($name);
        $entity->description = $description;

        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
            return $entity;
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
            return null;
        }
    }
}