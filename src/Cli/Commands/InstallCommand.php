<?php

namespace Duna\Plugin\Manager\Cli\Commands;

use Doctrine\ORM\Tools\ToolsException;
use Duna\Console\Command;
use Duna\Plugin\Manager\DI\Extension;

class InstallCommand extends Command
{
    public function checkInstalled()
    {
        return 1;
    }

    public function getDependentCommands()
    {
        return [];
    }

    public function getEntityMappings($onlyKey = false, $data = [])
    {
        return parent::getEntityMappings($onlyKey, Extension::entityMappings());
    }

    public function getExtension()
    {
        return Extension::class;
    }

    public function getTitle()
    {
        return 'Plugin manager';
    }

    public function runCommand()
    {
        $result = $this->getMetadataTables();
        try {
            $this->addMessageInfo('Creating database schema...');
            $this->schemaTool->updateSchema($result, true);
            return 0;
        } catch (ToolsException $e) {
            return 1;
        }
    }

    protected function configure()
    {
        $this->setName('plugin:manager:install')
            ->setDescription('Create table for installed plugins.');
    }
}