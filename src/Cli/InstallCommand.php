<?php

namespace Duna\Plugin\Manager\Cli;

use Duna\Console\DelegateCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends DelegateCommand
{
    /** @var \Kdyby\Doctrine\Tools\CacheCleaner @inject */
    public $cacheCleaner;

    /**
     * @return \Symfony\Component\Console\Command\Command
     */
    protected function createCommand()
    {
        return new Commands\InstallCommand();
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->cacheCleaner->invalidate();
    }

}