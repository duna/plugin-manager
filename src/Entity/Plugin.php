<?php

namespace Duna\Plugin\Manager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin")
 * @ORM\Entity
 */
class Plugin
{

	use \Kdyby\Doctrine\Entities\MagicAccessors;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="plugin_id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", length=128, nullable=false)
	 */
	protected $name;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", unique=true, nullable=false)
	 */
	protected $hash;

	/**
	 * @var type string
	 * 
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description = null;

	/**
	 * @var boolean
	 * 
	 * @ORM\Column(type="boolean")
	 */
	protected $template = false;

	public function getId()
	{
		return $this->id;
	}

}
