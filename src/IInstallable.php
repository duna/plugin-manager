<?php

namespace Duna\Plugin\Manager;


interface IInstallable
{
    public function install();
    public function uninstall();
}